<?php

namespace App;


class Course
{

    private $Bangla;
    private $gradeBangla;

    private $English;
    private $gradeEnglish;

    private $Math;
    private $gradeMath;

    public function setBangla($Bangla)
    {
        $this->Bangla = $Bangla;
         // bangla mark ke grade a convert korche

    }

    public function getBangla()
    {
        return $this->Bangla;
    }


    public function setgradeBangla()
    {
        $this->gradeBangla = $this->mark2grade($this->Bangla);
    }



    public function setEnglish($English)
    {
        $this->English = $English;
        // English mark ke grade a convert korche

    }

    public function getEnglish()
    {
        return $this->English;
    }

    public function setgradeEnglish()
    {
        $this->gradeEnglish = $this->mark2grade($this->English);
    }


    public function setMath($Math)
    {
        $this->Math = $Math;
         // Math mark ke grade a convert korche

    }

    public function getMath()
    {
        return $this->Math;
    }


    public function setgradeMath()
    {
        $this->gradeMath = $this->mark2grade($this->Math);

    }


    public function getGradeBangla()
    {
        return $this->gradeBangla;
    }

    public function getGradeEnglish()
    {
        return $this->gradeEnglish;
    }


    public function getGradeMath()
    {
        return $this->gradeMath;
    }


    public function mark2grade($mark)
    {

        switch($mark)
        {

            case ($mark>79) :
                $grade = "A+";
                break;
            case ($mark>69) :
                $grade = "A";
                break;
            case ($mark>59) :
                $grade = "A-";
                break;
            case ($mark>49) :
                $grade = "B";
                break;
            case ($mark>39) :
                $grade = "C";
                break;
            case ($mark>32) :
                $grade = "D";
                break;

            default: $grade = "F";

        }

        return $grade;

    }


}